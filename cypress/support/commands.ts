// add new command to the existing Cypress interface
declare global {
  namespace Cypress {
    interface Chainable {
      login: () => void;
    }
  }
}

export function login() {
  const query = `
      mutation Login {
          login(
              username: "${Cypress.env("USERNAME")}", 
              password: "${Cypress.env("PASSWORD")}"
          ) {
              token
          }
      }
    `;

  cy.request({
    method: "POST",
    url: Cypress.env("API_URL"),
    body: { query },
    headers: {
      "content-type": "application/json",
      Authorization: `Bearer ${Cypress.env("KITE_ANONYMOUS_TOKEN")}`,
    },
  }).then((res) => {
    localStorage.setItem("farmsetu_token", res.body.data.login.token);
  });
}

Cypress.Commands.overwrite("request", (originalFn, ...options) => {
  const optionsObject = options[0];

  if (optionsObject === Object(optionsObject)) {
    optionsObject.headers = {
      "content-type": "application/json",
      // "x-hasura-role": "agronomist",
      Authorization: `Bearer ${localStorage.getItem("farmsetu_token")}`,
      ...optionsObject.headers,
    };

    return originalFn(optionsObject);
  }

  return originalFn(...options);
});

// Cypress command to bypass login
Cypress.Commands.add("login", login);

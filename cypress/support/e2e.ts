// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import "./commands";
import "@cypress/code-coverage/support";

// Alternatively you can use CommonJS syntax:
// require('./commands')

/*
  This command is added to ignore the "ResizeObserver loop limit exceeded" error 
  which is not handled by cypress and is been advised to ignore by cypress maintainers
  Issue Links:
    https://github.com/cypress-io/cypress/issues/8418
    https://github.com/cypress-io/cypress/issues/22113
  Also, present in the cypress repository example app: 
    https://github.com/cypress-io/cypress/blob/develop/packages/app/cypress/component/support/index.ts#L52
*/
Cypress.on(
  "uncaught:exception",
  (err) => !err.message.includes("ResizeObserver loop limit exceeded")
);
